"use strict"

let baseUrl ="http://nowaunoweb.azurewebsites.net";
let playerName1;
let playerName2;
let playerName3;
let playerName4;
let player1;
let player2;
let player3;
let player4;
let drawCard;
let currentPlayer;
let nextPlayer;
let topCard;
let gameId;
let game;
let players= {};
let chooseColor;
let counter = 0;

$("#submitNames").click(gameStart)      // register Click-Event (gameStart()) on button in register form.
$(".leftBox").on('click', '#drawCard', handleDrawCard)      // register Click-Event(handleDrawCard()) on container of card stack. Delegated Event because stack does not exist in DOM at beginning.

// Before Game starts, just the input form for the player-names is shown. The game starts if all 4 names are filled in.
$(document).ready(function() {
    $('#nameModal').show()
});

function gameStart(e){
    let inputlist = $('#nameModal').find("input")
    let values =inputlist.map(function() { return this.value;}).get()   //get names from input fields and save values in set
    var set = new Set(values)
    
    if($("#inlineFormInputName1").val()=="" || $("#inlineFormInputName2").val()=="" || $("#inlineFormInputName3").val()=="" 
    || $("#inlineFormInputName4").val()=="" || set.size !=4) {  // if set.size != 4 the same name was entered twice / if val = "" a field is empty                     
    inputlist.each(function() {
        if($(this).val()=="") {
            $(this).toggleClass("invalid-input")
            }
    }) 
    alert("Please choose 4 different names.")
    } else {
        playerName1=$("#inlineFormInputName1").val(); 
        playerName2=$("#inlineFormInputName2").val();
        playerName3=$("#inlineFormInputName3").val();
        playerName4=$("#inlineFormInputName4").val();
        
        $("#nameFirst").text(playerName1)
        $("#nameSecond").text(playerName2)
        $("#nameThird").text(playerName3)
        $("#nameFourth").text(playerName4)
        
        // request for game start (sending names to server --> getting players, gameId, nextPlayer, topCard)
        var request = $.ajax({
            url: baseUrl + "/api/Game/Start",
            method: 'POST',
            data: JSON.stringify([    // convert JS object into JSON-string
                playerName1, playerName2, playerName3, playerName4,
            ]),
            contentType: 'application/json',     // datatype sent to server
            dataType: 'json'                     // datatype I want to receive
        })
        request.done(function(data){
            game = data;
            game.Players.forEach(function (playerServer) {   //iterate over players (from server) and save player object (from server) in local player object (key = name)
            players[playerServer.Player] = playerServer;
        });
           
            nextPlayer = game.NextPlayer;
            gameId = game.Id;
            topCard = game.TopCard; 
            player1 = players[playerName1]      //save local player object in new player 1-4 variable (for later use)
            player2 =  players[playerName2]
            player3 =  players[playerName3]
            player4 =  players[playerName4]
        
            currentPlayer = nextPlayer;     // save active player for highlighting

            $("#nameFirstChild").text(player1.Player)   // show playernames in scorebox
            $("#nameSecondChild").text(player2.Player)
            $("#nameThirdChild").text(player3.Player)
            $("#nameFourthChild").text(player4.Player)

            highlightActivePlayer(currentPlayer, nextPlayer)
            showTopCard(topCard);
            showAllPlayerCards()    // getting current player objects and show cards
            
        })
        request.fail(function(fail){ console.log('Error: ', fail); });
        
        $("#playground").show() //show playground
        $("#nameModal").hide()  // hide modal
    }
}

function highlightActivePlayer(currentPlayer, nextPlayer){  // compare current player with displayed name and set highlight on current players name
    $(".name").map(function(){
       if(this.textContent == nextPlayer){
        $(this).addClass("highlight");
        if(currentPlayer == nextPlayer)                     
        return;
       } 
       if(this.textContent == currentPlayer)
       $(this).removeClass("highlight");
    });
}

function showTopCardWildColor(value){   // if draw 4 or change color is played, new topCard is shown (colored draw 4 or choose color)
    let card;
    if(value == 13) {
        let cardSource = chooseColor.charAt(0).toLowerCase();
        card = "cards/wild4_" + cardSource + ".png";
       }
    if(value == 14) {
        let cardSource = chooseColor.charAt(0).toLowerCase();
        card = "cards/wild_" + cardSource + ".png";
    }
    $(".leftBox .stack:first-child").empty();
    $(".leftBox .stack:first-child").append("<img id='topCard' class='activeStackPic mr-5' src='"+ card + "'></img>"); 
    $(".leftBox .stack:nth-child(odd)").append("<img id='drawCard' class='activeStackPic mr-1' src='cards/back.png'></img>");
}

function showTopCard(newTopCard) {      // the card which is played is shown as new top card
    $(".leftBox .stack:first-child").empty();
    let cardSource = (newTopCard.Color.charAt(0).toLowerCase()).concat(newTopCard.Value);
    let card = "cards/" + cardSource + ".png";
    let back = "cards/back.png";
        
    $(".leftBox .stack:first-child").append("<img id='topCard' class='activeStackPic mr-5' src='"+ card + "'></img>"); 
    $(".leftBox .stack:nth-child(odd)").append("<img id='drawCard' class='activeStackPic mr-1' src='"+ back + "'></img>"); 
};

function handleDrawCard(){  // player has to draw a card
    let request = $.ajax({
        url: baseUrl + "/api/Game/DrawCard/"+gameId, // url + current gameId
        method: 'PUT',
        dataType: 'json'    // datatype to receive
    });
    request.done(function(data) {
        updatePlayer(data.NextPlayer)   // if request was successful, show new card
    });
    request.fail(function(data) {
       console.log("Request result in error.")
    });
}

function showAllPlayerCards () {
 
// sending getCards request for all players to server and update local players object

    let pl1 = $.ajax({
        url: baseUrl + '/api/Game/GetCards/' + gameId + '?playerName=' + playerName1,
        method: 'GET',
        dataType: 'json'
    })
    pl1.done(function(data) {
        player1.Cards=data.Cards;   // update local cards for all players 
        player1.Score = data.Score; // update local score for all players
    })
  
    let pl2 = $.ajax({
        url: baseUrl + '/api/Game/GetCards/' + gameId + '?playerName=' + playerName2,
        method: 'GET',
        dataType: 'json'
    })
    pl2.done(function(data) {
        player2.Cards=data.Cards;
        player2.Score = data.Score;
    });
  
    let pl3 = $.ajax({
        url: baseUrl + '/api/Game/GetCards/' + gameId + '?playerName=' + playerName3,
        method: 'GET',
        dataType: 'json'
    })
    pl3.done(function(data) {
        player3.Cards=data.Cards;
        player3.Score = data.Score;
    });
 
    let pl4 = $.ajax({
        url: baseUrl + '/api/Game/GetCards/' + gameId + '?playerName=' + playerName4,
        method: 'GET',
        dataType: 'json'
    })
    pl4.done(function(data) {
        player4.Cards=data.Cards;
        player4.Score = data.Score;
    });
 
    $.when(pl1, pl2, pl3, pl4).then(function() { // if all 4 requests are done, iterate over cards of all players and display cards

        player1.Cards.map(function(card){ 
            let cardSource = (card.Color.charAt(0).toLowerCase()).concat(card.Value)
            let singlePicture = $("<img class='activePic' id='activePicture' src='cards/"+cardSource+".png'></img>");
            let pic= $("#firstRow").append(singlePicture); // get img and append it to DOM and to players row
            if(counter < 28){       // extended animation at game start
                animateCards(pic);
            } else {
                animateCardsDuringGame(pic) // faster animation
            }
            $("#firstChild").text('') // remove current score 
            $("#firstChild").text(player1.Score) // add current score
            if(nextPlayer == player1.Player) { // if player1 = current player register click-event on cards to call function playCard()
                singlePicture.on('click', function() {
                        playCard(card);
                })
                animateSingleCard(singlePicture)
               } else {
                   return;
               }
          })
       
          player2.Cards.map(function(card){ 
           let cardSource = (card.Color.charAt(0).toLowerCase()).concat(card.Value)
           let singlePicture = $("<img class='activePic' id='activePicture' src='cards/"+cardSource+".png'></img>");
           let pic= $("#secondRow").append(singlePicture);
           if(counter < 28){
            animateCards(pic);
            } else {
                animateCardsDuringGame(pic)
            }
           $("#secondChild").text('')
           $("#secondChild").text(player2.Score)
           if(nextPlayer == player2.Player) {
               singlePicture.on('click', function() {
                       playCard(card);
               })
               animateSingleCard(singlePicture)
              } else {
                  return;
              }
         })
       
         player3.Cards.map(function(card){ 
           let cardSource = (card.Color.charAt(0).toLowerCase()).concat(card.Value)
           let singlePicture = $("<img class='activePic' id='activePicture' src='cards/"+cardSource+".png'></img>");
           let pic= $("#thirdRow").append(singlePicture);
           if(counter < 28){
            animateCards(pic);
            } else {
                animateCardsDuringGame(pic)
            }
           $("#thirdChild").text('')
           $("#thirdChild").text(player3.Score)
           if(nextPlayer == player3.Player) {
               singlePicture.on('click', function() {
                       playCard(card);
               })
               animateSingleCard(singlePicture)
              } else {
                  return;
              }
         })
       
         player4.Cards.map(function(card){ 
           let cardSource = (card.Color.charAt(0).toLowerCase()).concat(card.Value)
           let singlePicture = $("<img class='activePic' id='activePicture' src='cards/"+cardSource+".png'></img>");
           let pic= $("#fourthRow").append(singlePicture);
           if(counter < 28){
            animateCards(pic);
            } else {
                animateCardsDuringGame(pic)
            }
           $("#fourthChild").text('')
           $("#fourthChild").text(player4.Score)
           if(nextPlayer == player4.Player) {
               singlePicture.on('click', function() {
                       playCard(card);
               })
               animateSingleCard(singlePicture)
              } else {
                  return;
              }
         })
    }) 
}

function sendWildCardRequest(card) { // if played card is wildcard (draw 4 and change color) a wildcard request is sent to server
    let url =baseUrl + '/api/game/playCard/' + gameId + '?value=' + card.Value + '&color=' + card.Color + '&wildColor='+chooseColor; // url + choosen color
    let request = $.ajax({
        url: url,
        method: 'PUT',
        dataType: 'json'
    });
    // if request is done show player cards and update top card to wild + color
    request.done(function(data){updatePlayer(data.Player); showTopCardWildColor(card.Value)})
    $("#red").off()
    $("#green").off()
    $("#blue").off()
    $("#yellow").off()  // remove click event from color-buttons
}

function playCard (card) {  

    // if card is wildcard (draw 4 + change color) choose-color-modal is shown and click-event is registered on each color-button
    // click-event: choosen color is stored in local variable, modal is hidden again and wildcard request is sent to server
            if (card.Value == 13 || card.Value == 14) {
                $("#chooseColor").show()
                $("#playground").css( 'opacity','0.5')
                $("#green").click(function() {chooseColor = "Green"; $("#chooseColor").hide(); sendWildCardRequest(card); $("#playground").css( 'opacity','1') })
                $("#red").click(function() {chooseColor = "Red",$("#chooseColor").hide(); sendWildCardRequest(card); $("#playground").css( 'opacity','1') })
                $("#yellow").click(function() {chooseColor = "Yellow",$("#chooseColor").hide(); sendWildCardRequest(card); $("#playground").css( 'opacity','1') })
                $("#blue").click(function() {chooseColor = "Blue",$("#chooseColor").hide(); sendWildCardRequest(card); $("#playground").css( 'opacity','1') })
                
                return;
            } 
            // sending playCard request to server for all other cards
            let url =baseUrl + '/api/game/playCard/' + gameId + '?value=' + card.Value + '&color=' + card.Color + '&wildColor=';
            let request = $.ajax({ 
                url: url,
                method: 'PUT',
                dataType: 'json'
            });
            request.done(function(data) { // if request is done check if card was valid (if not --> alert), else: new topCard is played card, update all players cards
                if(data.Player == undefined){
                    alert("You must not play this card. Please choose another one or take one.")
                } else {      
                    showTopCard(card);
                    updatePlayer(data.Player);
                    }
                });
            request.fail(function(data) {
                console.log("Server denied card", card, data);
            });
}

function animateCards(pic){ // START-ANIMATION: search all images, rotate for 2 seconds
    counter++;
    pic.find("img").animate({
        marginLeft: "30px",
        rotation: 360,
      },{
        duration: 200,
        step: function(now) {
          $(this).css({"transform": "rotate("+now+"deg)"});
        },
        complete: function(){
          $(this).animate({
            marginLeft: "8px",
          }, 50);
        }
      });
}

function animateCardsDuringGame(pic){ // push cards right and left
    pic.find("img").animate({
        marginLeft: "10px",
      },{
        duration: 80,
        complete: function(){
          $(this).animate({
            marginLeft: "10px",
          }, 50);
        }
      });
}

let updatePlayer = function (player) {      // anonymous function 

    // if next player isn't updated by server --> player has won. Modal with score is shown - options: start new game or exit
    if (player === nextPlayer) {

        $("#playground").empty();
        $('#gameOver').show();
        $('#winner').text("Congrats " + nextPlayer + " - you won!");
        players[nextPlayer].Score = 0;
        $("#scoreFirst").text(players[playerName1].Player);    // show player names for scorebox
        $("#scoreSecond").text(players[playerName2].Player);
        $("#scoreThird").text(players[playerName3].Player);
        $("#scoreFourth").text(players[playerName4].Player);

        $("#score1").text(players[playerName1].Score);        // show scores
        $("#score2").text(players[playerName2].Score);
        $("#score3").text(players[playerName3].Score);
        $("#score4").text(players[playerName4].Score);

        $('#newGame').click(function() {    // start new game
            location.reload();
        });
        $('#exitGame').click(function() {   // close game
            window.close();
        });
        return;
    }

    // UPDATE during game: save next player from server in local currentPlayer variable and set highlighting on active player
    currentPlayer = nextPlayer;
    nextPlayer = player;
    highlightActivePlayer(currentPlayer, nextPlayer);
    $("#firstRow").empty()      // remove current cards before displaying new cards from server
    $("#secondRow").empty()
    $("#thirdRow").empty()
    $("#fourthRow").empty()

    showAllPlayerCards();   // update card images for all players
}

function animateSingleCard(singlePicture) {
    singlePicture.on('mouseover', function() {
        singlePicture.addClass('hover')
    })
    singlePicture.on('mouseleave', function() {
        singlePicture.removeClass('hover')
    })
}